console.log('app.js');

function validateInput(){
	if(document.getElementById('email').value===""){
		alert('Debe indicar el email antes de cargar el archivo.');
		return false;
	}else{
		return true;
	}
}

var formData = new FormData();

$(document).ready(function () {
    console.log('ready');
    $('#input_file').on('change', () => {
        console.log('getFile');
        let input = document.getElementById('input_file');
        formData.append('file', input.files[0]);
		
		let email = document.getElementById('email');
		formData.append('email',email.value);
		
        document.getElementById('btn_upload').disabled=false;
		
		/* 60.000 milisegundos es 1 minuto*/
		setTimeout(document.getElementById('btn_upload').click(), 60000);
    });
    $('#btn_upload').on('click', () => {
        console.log('uploadFile');
		
		
$.ajax({
    url: 'http://localhost:8080/import/file',
    data: formData,
    type: 'POST',
    contentType: false, 
    processData: false,
	success: function(){
		document.getElementById('email').value="";
		alert('El archivo ha sido enviado al servidor.\n Los resultados se enviaran al email suministrado');
	},
	error: function(){
		alert('El envio ha Fallado. Comprueba que el servidor este disponible.');
	}
});
	
			document.getElementById('btn_upload').disabled=true;
    });
	
});