package com.alphas.springrest.springapp.dto;

import com.alphas.springrest.springapp.model.Marca;

import java.util.List;

/**
 * Data Transfer Object para Entidad Marca
 */
public class MarcaDto {
    Long id;
    String nombre;
    String descripcion;

    List<MarcaDto> marcaDtoList;

    public MarcaDto(Long id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public MarcaDto(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<MarcaDto> getMarcaDtoList() {
        return marcaDtoList;
    }

    public void setMarcaDtoList(List<MarcaDto> marcaDtoList) {
        this.marcaDtoList = marcaDtoList;
    }

    public static MarcaDto fromModel(Marca marca){
        if(marca == null){
            return null;
        }

        return new MarcaDto(marca.getId(), marca.getNombre(), marca.getDescripcion());
    }

    @Override
    public String toString() {
        return "MarcaDto{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
