package com.alphas.springrest.springapp.dto;

import com.alphas.springrest.springapp.model.Vehiculo;

import java.util.List;

public class VehiculoDto {
    Long id;
    String placa;
    String color;
    Double capacidad;
    MarcaDto marcaDto;
    ModeloDto modeloDto;

    List<VehiculoDto> vehiculoDtoList;

    public VehiculoDto(Long id, String placa, String color, Double capacidad, MarcaDto marcaDto, ModeloDto modeloDto) {
        this.id = id;
        this.placa = placa;
        this.color = color;
        this.capacidad = capacidad;
        this.marcaDto = marcaDto;
        this.modeloDto = modeloDto;
    }

    public VehiculoDto(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Double capacidad) {
        this.capacidad = capacidad;
    }

    public MarcaDto getMarcaDto() {
        return marcaDto;
    }

    public void setMarcaDto(MarcaDto marcaDto) {
        this.marcaDto = marcaDto;
    }

    public ModeloDto getModeloDto() {
        return modeloDto;
    }

    public void setModeloDto(ModeloDto modeloDto) {
        this.modeloDto = modeloDto;
    }

    public List<VehiculoDto> getVehiculoDtoList() {
        return vehiculoDtoList;
    }

    public void setVehiculoDtoList(List<VehiculoDto> vehiculoDtoList) {
        this.vehiculoDtoList = vehiculoDtoList;
    }

    public static VehiculoDto fromModel(Vehiculo vehiculo){
        if(vehiculo == null){
            return null;
        }

        return new VehiculoDto(vehiculo.getId(), vehiculo.getPlaca(), vehiculo.getColor(), vehiculo.getCapacidad(),
                MarcaDto.fromModel(vehiculo.getMarca()),ModeloDto.fromModel(vehiculo.getModelo()));
    }

    @Override
    public String toString() {
        return "VehiculoDto{" +
                "id=" + id +
                ", placa='" + placa + '\'' +
                ", color='" + color + '\'' +
                ", capacidad=" + capacidad +
                ", marcaDto=" + marcaDto +
                ", modeloDto=" + modeloDto +
                '}';
    }
}
