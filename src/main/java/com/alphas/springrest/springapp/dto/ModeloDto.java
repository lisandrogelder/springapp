package com.alphas.springrest.springapp.dto;

import com.alphas.springrest.springapp.model.Marca;
import com.alphas.springrest.springapp.model.Modelo;

import java.util.List;

public class ModeloDto {
    Long id;
    String nombre;
    String descripcion;

    List<ModeloDto> modeloDtoList;

    public ModeloDto(Long id, String nombre, String descripcion) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public ModeloDto(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ModeloDto> getModeloDtoList() {
        return modeloDtoList;
    }

    public void setModeloDtoList(List<ModeloDto> modeloDtoList) {
        this.modeloDtoList = modeloDtoList;
    }

    public static ModeloDto fromModel(Modelo modelo){
        if(modelo == null){
            return null;
        }

        return new ModeloDto(modelo.getId(), modelo.getNombre(), modelo.getDescripcion());
    }

    @Override
    public String toString() {
        return "ModeloDto{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                '}';
    }
}
