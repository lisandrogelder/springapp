package com.alphas.springrest.springapp.controller;


import com.alphas.springrest.springapp.common.BackendResponse;
import com.alphas.springrest.springapp.common.enums.ResponseStatus;
import com.alphas.springrest.springapp.dto.ModeloDto;
import com.alphas.springrest.springapp.service.ModeloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/modelo")
public class ModeloController {
    //servicios
    @Autowired
    public ModeloService modeloService;

    @GetMapping("/list")
    public BackendResponse getModeloList(){
        return new BackendResponse(modeloService.getAll());
    }

    @PostMapping("/save")
    public BackendResponse saveModelo(@RequestBody ModeloDto modeloDto){
        try{
            return new BackendResponse(modeloService.save(modeloDto));
        }catch(Exception e){
            e.printStackTrace();
            return new BackendResponse(ResponseStatus.ERROR, e.getMessage());
        }
    }

    @PostMapping("/saveAll")
    public BackendResponse saveModeloList(@RequestBody List<ModeloDto> modeloDtoList){
        try{
            return new BackendResponse(modeloService.saveList(modeloDtoList));
        }catch(Exception e){
            e.printStackTrace();
            return new BackendResponse(ResponseStatus.ERROR, e.getMessage());
        }
    }
}
