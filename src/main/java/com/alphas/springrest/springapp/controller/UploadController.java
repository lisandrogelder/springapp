package com.alphas.springrest.springapp.controller;

import com.alphas.springrest.springapp.common.BackendResponse;
import com.alphas.springrest.springapp.service.ExcelReaderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.util.unit.DataSize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.MultipartConfigElement;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/import")
public class UploadController {

    private String filePath="./upload";

    //services
    private final ExcelReaderService excelReaderService;

    @Autowired
    public UploadController(ExcelReaderService excelReaderService) {
        this.excelReaderService = excelReaderService;
    }


    @SuppressWarnings("deprecation")
    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize(DataSize.parse("124MB"));
        factory.setMaxRequestSize(DataSize.parse("124MB"));
        return factory.createMultipartConfig();
    }

    @PostMapping("/file")
    public BackendResponse uploadFile(MultipartFile file, @RequestParam(value = "email") String email) throws IOException {
        String fileLocation = saveMultipartFile(file);
        String fileName = file.getOriginalFilename();
        try {
            return excelReaderService.importExcelFile(fileLocation, fileName, email);
        } catch (Exception e) {
            e.printStackTrace();
            return new BackendResponse(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    private String saveMultipartFile(MultipartFile file) throws IOException {
        InputStream in = file.getInputStream();
        String fileLocation = filePath + file.getOriginalFilename();
        FileOutputStream f = new FileOutputStream(fileLocation);
        int ch;
        while ((ch = in.read()) != -1) {
            f.write(ch);
        }
        f.flush();
        f.close();
        return fileLocation;
    }
}
