package com.alphas.springrest.springapp.controller;

import com.alphas.springrest.springapp.common.BackendResponse;
import com.alphas.springrest.springapp.common.enums.ResponseStatus;
import com.alphas.springrest.springapp.dto.VehiculoDto;
import com.alphas.springrest.springapp.service.VehiculoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/vehiculo")
public class VehiculoController {
    //servicios
    @Autowired
    public VehiculoService vehiculoService;

    @GetMapping("/list")
    public BackendResponse getVehiculoList(){
        return new BackendResponse(vehiculoService.getAll());
    }

    @PostMapping("/save")
    public BackendResponse saveVehiculo(@RequestBody VehiculoDto vehiculoDto){
        try{
            return new BackendResponse(vehiculoService.save(vehiculoDto));
        }catch(Exception e){
            e.printStackTrace();
            return new BackendResponse(ResponseStatus.ERROR, e.getMessage());
        }
    }

    @PostMapping("/saveAll")
    public BackendResponse saveVehiculoList(@RequestBody List<VehiculoDto> vehiculoDtoList){
        try{
            return new BackendResponse(vehiculoService.saveList(vehiculoDtoList));
        }catch(Exception e){
            e.printStackTrace();
            return new BackendResponse(ResponseStatus.ERROR, e.getMessage());
        }
    }
}
