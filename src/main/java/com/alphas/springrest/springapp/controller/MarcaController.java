package com.alphas.springrest.springapp.controller;

import com.alphas.springrest.springapp.common.BackendResponse;
import com.alphas.springrest.springapp.common.enums.ResponseStatus;
import com.alphas.springrest.springapp.dto.MarcaDto;
import com.alphas.springrest.springapp.service.MarcaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/marca")
public class MarcaController {

    //servicios
    @Autowired
    public MarcaService marcaService;

    @GetMapping("/list")
    public BackendResponse getMarcaList(){
        return new BackendResponse(marcaService.getAll());
    }

    @PostMapping("/save")
    public BackendResponse saveMarca(@RequestBody MarcaDto marcaDto){
        try{
            return new BackendResponse(marcaService.save(marcaDto));
        }catch(Exception e){
            e.printStackTrace();
            return new BackendResponse(ResponseStatus.ERROR, e.getMessage());
        }
    }

    @PostMapping("/saveAll")
    public BackendResponse saveMarcaList(@RequestBody List<MarcaDto> marcaDtoList){
        try{
            return new BackendResponse(marcaService.saveList(marcaDtoList));
        }catch(Exception e){
            e.printStackTrace();
            return new BackendResponse(ResponseStatus.ERROR, e.getMessage());
        }
    }
}
