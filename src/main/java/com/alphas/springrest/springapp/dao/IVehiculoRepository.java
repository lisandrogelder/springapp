package com.alphas.springrest.springapp.dao;

import com.alphas.springrest.springapp.model.Marca;
import com.alphas.springrest.springapp.model.Vehiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository("vehiculoRepository")
public interface IVehiculoRepository extends JpaRepository<Vehiculo, Long> {

    @Query(value="Select * From vehiculo", nativeQuery = true)
    List<Vehiculo> findAll();

    @Query(value="Select * From vehiculo Where id_vehiculo=:id_vehiculo", nativeQuery = true)
    Optional<Vehiculo> findById(@Param("id_vehiculo") Long id);

    @Query(value="Select * From vehiculo Where placa=:placa", nativeQuery = true)
    Optional<Vehiculo> findByPlaca(@Param("placa") String placa);

    @Transactional
    @Modifying
    @Query(value="Insert Into vehiculo(id_vehiculo,placa,color,capacidad,id_marca,id_modelo)Values(:id_vehiculo,:placa,:color,:capacidad,:id_marca,:id_modelo)",
            nativeQuery = true)
    void create(@Param("id_vehiculo") Long id,
                @Param("placa") String placa,
                @Param("color") String color,
                @Param("capacidad") Double capacidad,
                @Param("id_marca") Long idMarca,
                @Param("id_modelo") Long idModelo);

    @Transactional
    @Modifying
    @Query(value="Update vehiculo Set placa=:placa,color=:color,capacidad=:capacidad,id_marca=:id_marca,id_modelo=:id_modelo Where id_vehiculo=:id_vehiculo",
            nativeQuery = true)
    Integer update(@Param("id_vehiculo") Long id,
                @Param("placa") String placa,
                @Param("color") String color,
                @Param("capacidad") Double capacidad,
                @Param("id_marca") Long idMarca,
                @Param("id_modelo") Long idModelo);
}
