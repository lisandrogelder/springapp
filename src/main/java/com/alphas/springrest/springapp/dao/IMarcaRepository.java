package com.alphas.springrest.springapp.dao;

import com.alphas.springrest.springapp.model.Marca;
import com.alphas.springrest.springapp.model.Modelo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository("marcaRepository")
public interface IMarcaRepository extends JpaRepository<Marca, Long> {

    @Query(value="Select * From marca", nativeQuery = true)
    List<Marca> findAll();

    @Query(value="Select * From marca Where id_marca=:id_marca", nativeQuery = true)
    Optional<Marca> findById(@Param("id_marca") Long id);

    @Query(value="Select * From marca Where nombre=:nombre", nativeQuery = true)
    Optional<Marca> findByNombre(@Param("nombre") String nombre);

    @Transactional
    @Modifying
    @Query(value="Insert Into marca(id_marca,nombre,descripcion)Values(:id_marca,:nombre,:descripcion)",
            nativeQuery = true)
    void create(@Param("id_marca") Long id,
                @Param("nombre") String nombre,
                @Param("descripcion") String descripcion);

    @Transactional
    @Modifying
    @Query(value="Update marca Set nombre=:nombre,descripcion=:descripcion Where id_marca=:id_marca",
            nativeQuery = true)
    Integer update(@Param("id_marca") Long id,
                @Param("nombre") String nombre,
                @Param("descripcion") String descripcion);
}
