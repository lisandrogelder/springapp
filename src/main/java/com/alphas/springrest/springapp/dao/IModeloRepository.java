package com.alphas.springrest.springapp.dao;

import com.alphas.springrest.springapp.model.Modelo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository("modeloRepository")
public interface IModeloRepository extends JpaRepository<Modelo, Long> {

    @Query(value="Select * From modelo", nativeQuery = true)
    List<Modelo> findAll();

    @Query(value="Select * From modelo Where id_modelo = :id_modelo", nativeQuery = true)
    Optional<Modelo> findById(@Param("id_modelo") Long id);

    @Query(value="Select * From modelo Where nombre = :nombre", nativeQuery = true)
    Optional<Modelo> findByNombre(@Param("nombre") String nombre);

    @Transactional
    @Modifying
    @Query(value="Insert Into modelo(id_modelo,nombre,descripcion)Values(:id_modelo,:nombre,:descripcion)",
            nativeQuery = true)
    void create(@Param("id_modelo") Long id,
                @Param("nombre") String nombre,
                @Param("descripcion") String descripcion);

    @Transactional
    @Modifying
    @Query(value="Update modelo Set nombre=:nombre,descripcion=:descripcion Where id_modelo=:id_modelo",
            nativeQuery = true)
    int update(@Param("id_modelo") Long id,
                @Param("nombre") String nombre,
                @Param("descripcion") String descripcion);
}
