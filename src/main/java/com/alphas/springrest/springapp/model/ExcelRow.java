package com.alphas.springrest.springapp.model;

public class ExcelRow {
    //numero de fila
    private Integer rowNumber;

    //fila de cabeceras
    public static final int HEADERS_ROW = 1;

    //indice de las columnas de una fila
    public static final int PLACA = 0;
    public static final int MARCA = 1;
    public static final int MODELO = 2;
    public static final int COLOR = 3;
    public static final int CAPACIDAD = 4;

    //campos de columnas
    private String placa;
    private String marca;
    private String modelo;
    private String color;
    private Double capacidad;

    public Integer getRowNumber() {
        return rowNumber;
    }

    public void setRowNumber(Integer rowNumber) {
        this.rowNumber = rowNumber;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Double capacidad) {
        this.capacidad = capacidad;
    }

    public static int getLastColumnIndex() {
        return CAPACIDAD;
    }
}
