package com.alphas.springrest.springapp.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "vehiculo")
public class Vehiculo implements Serializable {

    @Id
    @Column(name = "id_vehiculo")
    private Long id;

    @Column(name = "color")
    private String color;

    @Column(name = "placa")
    private String placa;

    @Column(name = "capacidad")
    private Double capacidad;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_marca", referencedColumnName = "id_marca", nullable = false)
    Marca marca;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_modelo", referencedColumnName = "id_modelo", nullable = false)
    Modelo modelo;

    public Vehiculo(Long id, String color, String placa, Double capacidad, Marca marca, Modelo modelo) {
        this.id = id;
        this.color = color;
        this.placa = placa;
        this.capacidad = capacidad;
        this.marca = marca;
        this.modelo = modelo;
    }

    public Vehiculo(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public Double getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(Double capacidad) {
        this.capacidad = capacidad;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public Modelo getModelo() {
        return modelo;
    }

    public void setModelo(Modelo modelo) {
        this.modelo = modelo;
    }
}
