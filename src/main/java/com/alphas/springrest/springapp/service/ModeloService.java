package com.alphas.springrest.springapp.service;

import com.alphas.springrest.springapp.dao.IModeloRepository;
import com.alphas.springrest.springapp.dto.ModeloDto;
import com.alphas.springrest.springapp.model.Modelo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("modeloService")
public class ModeloService {
    private static final Logger log = LogManager.getLogger(ModeloService.class);

    //repositorios
    private final IModeloRepository modeloRepository;

    //constructor
    @Autowired
    public ModeloService(IModeloRepository modeloRepository){
        this.modeloRepository = modeloRepository;
    }

    public List<ModeloDto> getAll(){
        return modeloRepository.findAll().stream().map(ModeloDto::fromModel).collect(Collectors.toList());
    }

    public ModeloDto getById(Long id){
        if(id == null){
            return null;
        }

        Optional<Modelo> modelo = modeloRepository.findById(id);
        if(modelo == null || !modelo.isPresent()) {
            return null;
        }

        return ModeloDto.fromModel(modelo.get());
    }

    public ModeloDto getByNombre(String nombre){
        if(nombre == null){
            return null;
        }

        Optional<Modelo> modelo = modeloRepository.findByNombre(nombre);
        if(modelo == null || !modelo.isPresent()) {
            return null;
        }

        return ModeloDto.fromModel(modelo.get());
    }

    public ModeloDto save(ModeloDto modeloDto){
        if(modeloDto == null){
            return null;
        }

        if(modeloDto.getId() == null){
            Optional<Modelo> temp = modeloRepository.findByNombre(modeloDto.getNombre());
            if(temp == null || !temp.isPresent()) {
                log.info("doesnt exist: ");
                log.info(modeloDto.toString());
                return create(modeloDto);
            }
            modeloDto.setId(temp.get().getId());

            log.info("dto: ");
            log.info(modeloDto.toString());
            return update(modeloDto);
        }
        return update(modeloDto);
    }

    public List<ModeloDto> saveList(List<ModeloDto> modeloDtoList){
        return modeloDtoList.stream().filter(Objects::nonNull).map(modeloDto -> save(modeloDto))
                .collect(Collectors.toList());
    }

    public ModeloDto create(ModeloDto modeloDto){
        modeloRepository.create(modeloDto.getId(), modeloDto.getNombre(), modeloDto.getDescripcion());
        return getByNombre(modeloDto.getNombre());
    }

    public ModeloDto update(ModeloDto modeloDto){
        Integer updatedrows = modeloRepository.update(modeloDto.getId(), modeloDto.getNombre(), modeloDto.getDescripcion());
        if(updatedrows != null && updatedrows > 0){
            return modeloDto;
        }
        return null;
    }
}
