package com.alphas.springrest.springapp.service;

import com.alphas.springrest.springapp.common.BackendResponse;
import com.alphas.springrest.springapp.common.enums.ResponseStatus;
import com.alphas.springrest.springapp.dao.IMarcaRepository;
import com.alphas.springrest.springapp.dao.IModeloRepository;
import com.alphas.springrest.springapp.dao.IVehiculoRepository;
import com.alphas.springrest.springapp.dto.MarcaDto;
import com.alphas.springrest.springapp.dto.ModeloDto;
import com.alphas.springrest.springapp.dto.VehiculoDto;
import com.alphas.springrest.springapp.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service("excelReaderService")
public class ExcelReaderService {
    private static final Logger log = LogManager.getLogger(ExcelReaderService.class);

    //excel
    List<ExcelRow> vehiculoRowList = new ArrayList<>();
    List<String> headers = new ArrayList<>();

    //listas
    List<Vehiculo> vehiculoList = new ArrayList<>();
    List<Vehiculo> vehiculoToCreateList = new ArrayList<>();
    List<VehiculoDto> vehiculoDtoList = new ArrayList<>();
    List<Modelo> modeloList = new ArrayList<>();
    List<Modelo> modeloToCreateList = new ArrayList<>();
    List<ModeloDto> modeloDtoList = new ArrayList<>();
    List<Marca> marcaList = new ArrayList<>();
    List<Marca> marcaToCreateList = new ArrayList<>();
    List<MarcaDto> marcaDtoList = new ArrayList<>();

    //errores
    List<Map<String,Object>> errorsByType = new ArrayList<>();
    List<String> errorList = new ArrayList<>();

    //campos de la clase
    String fileName;
    String email;

    //repositorios
    private final IVehiculoRepository vehiculoRepository;
    private final IModeloRepository modeloRepository;
    private final IMarcaRepository marcaRepository;

    //servicios
    private final ModeloService modeloService;
    private final MarcaService marcaService;
    private final VehiculoService vehiculoService;
    private final MailService mailService;

    @Autowired
    public ExcelReaderService(IVehiculoRepository vehiculoRepository, IModeloRepository modeloRepository,
                              IMarcaRepository marcaRepository, ModeloService modeloService, MarcaService marcaService,
                              VehiculoService vehiculoService, MailService mailService){
        this.vehiculoRepository = vehiculoRepository;
        this.modeloRepository = modeloRepository;
        this.marcaRepository = marcaRepository;
        this.modeloService = modeloService;
        this.marcaService = marcaService;
        this.vehiculoService = vehiculoService;
        this.mailService = mailService;
    }

    /**
     *
     * @param fileLocation
     * @param fileName
     * @return
     */
    public BackendResponse importExcelFile(String fileLocation, String fileName, String email) throws IOException {
        this.fileName = fileName;
        this.email = email;
        parseExcelFile(fileLocation);

        if(!errorList.isEmpty()){
            sendMail();
            return new BackendResponse(ResponseStatus.ERROR,errorList);
        }

        if(!errorsByType.isEmpty()){
            parseBodyErrors();
            sendMail();
            return new BackendResponse(ResponseStatus.ERROR,errorList);
        }

        processExcelFile();

        if(errorList.isEmpty()){
            saveData();
        }

        if(!errorList.isEmpty()){
            sendMail();
            return new BackendResponse(ResponseStatus.ERROR,errorList);
        }
        sendMail();
        return new BackendResponse("Archivo Procesado.");
    }

    private void parseExcelFile(String fileLocation) throws IOException {
        Sheet sheet = readFile(fileLocation);

        //iterar todas las lineas del archivo
        for(Row row : sheet){
            //si toda la columna esta vacia, continuar a la siguiente columna
            if(isEmptyRow(row)){
                continue;
            }

            //validar y almacenar cabeceras del archivo
            if(row.getRowNum() <= ExcelRow.HEADERS_ROW){
                setHeaders(row);
                continue;
            }

            //si hay errores en la validacion de cabeceras culminar el proceso
            if(!errorList.isEmpty()) {
                return;
            }

            //crear objeto para almacenar los valores de la fila iterada
            ExcelRow excelRow = parseRow(row);
            if(excelRow != null){
                vehiculoRowList.add(excelRow);
            }
        }
    }

    private void setHeaders(Row row){
        for(int columnIndex = 0; columnIndex <= ExcelRow.getLastColumnIndex(); columnIndex++){
            Cell cell = row.getCell(columnIndex, Row.RETURN_BLANK_AS_NULL);
            if(cell == null){
                errorList.add("Cabecera: columna " + (columnIndex+1) + "no definida");
                return;
            }
            String value = cell.getStringCellValue();
            headers.add(value);
        }
        if(headers.isEmpty()){
            errorList.add("Cabeceras no definidas");
        }
    }

    private static boolean isEmptyRow(Row row){
        boolean isEmptyRow = true;
        for(int columnIndex = 0; columnIndex <= ExcelRow.getLastColumnIndex(); columnIndex++){
            Cell cell = row.getCell(columnIndex, Row.RETURN_BLANK_AS_NULL);
            if(cell != null){
                isEmptyRow = false;
                break;
            }
        }
        return isEmptyRow;
    }

    private ExcelRow parseRow(Row row){
        ExcelRow excelRow = new ExcelRow();
        excelRow.setRowNumber(row.getRowNum() + 1);

        //si hay algun valor nulo/vacio en la fila, agregar la fila a un archivo de error
        for(int columnIndex = 0; columnIndex <= ExcelRow.getLastColumnIndex(); columnIndex++){
            Cell cell = row.getCell(columnIndex, Row.RETURN_BLANK_AS_NULL);
            if (cell == null) {
                if (row.getPhysicalNumberOfCells() == 0) {
                    errorList.add("End of Data");
                    return null;
                } else {
                    addGroupedError(columnIndex,excelRow,("Columna "+headers.get(columnIndex)+" No Definida"));
                    continue;
                }
            }

            //sino, mapear los valores de la fila en el objeto excelRow
            switch(columnIndex){
                case ExcelRow.PLACA:
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    excelRow.setPlaca(cell.getStringCellValue());
                    break;
                case ExcelRow.MARCA:
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    excelRow.setMarca(cell.getStringCellValue());
                    break;
                case ExcelRow.MODELO:
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    excelRow.setModelo(cell.getStringCellValue());
                    break;
                case ExcelRow.COLOR:
                    cell.setCellType(Cell.CELL_TYPE_STRING);
                    excelRow.setColor(cell.getStringCellValue());
                    break;
                case ExcelRow.CAPACIDAD:
                    cell.setCellType(Cell.CELL_TYPE_NUMERIC);
                    excelRow.setCapacidad(cell.getNumericCellValue());
                    break;
            }
        }
        return excelRow;
    }

    private void processExcelFile(){
        //se buscan los registros auiliares para tener mapeado y reducir los accesos a db
        modeloList = modeloRepository.findAll();
        marcaList = marcaRepository.findAll();
        vehiculoList = vehiculoRepository.findAll();

        for(ExcelRow excelRow : vehiculoRowList){
            log.info("Procesando Fila " + excelRow.getRowNumber());
            Marca marca = getOrCreateMarca(excelRow.getMarca());
            Modelo modelo = getOrCreateModelo(excelRow.getModelo());
            getOrCreateVehiculo(marca,modelo,excelRow);
        }
    }

    private Marca getOrCreateMarca(String nombreMarca){
        //verificar si ya existe en la lista de cache
        if(!marcaList.isEmpty()){
            Optional<Marca> temp = marcaList.stream().filter(marca -> marca.getNombre()
                    .equalsIgnoreCase(nombreMarca)).findFirst();
            if(temp != null && temp.isPresent()){
                return temp.get();
            }
        }

        //buscar en la lista de registros pendientes por crear
        if(!marcaToCreateList.isEmpty()){
            Optional<Marca> temp = marcaToCreateList.stream().filter(marca -> marca.getNombre()
                    .equalsIgnoreCase(nombreMarca)).findFirst();
            if(temp != null && temp.isPresent()){
                return temp.get();
            }
        }

        //sino esta en la lista ni en la db, crear el registro nuevo
        Marca marca = new Marca();
        marca.setNombre(nombreMarca);
        marca.setDescripcion(nombreMarca);
        marcaToCreateList.add(marca);
        return marca;
    }

    private Modelo getOrCreateModelo(String nombreModelo){
        //verificar si ya existe en la lista de cache
        if(!modeloList.isEmpty()){
            Optional<Modelo> temp = modeloList.stream().filter(modelo -> modelo.getNombre()
                    .equalsIgnoreCase(nombreModelo)).findFirst();
            if(temp != null && temp.isPresent()){
                return temp.get();
            }
        }

        //buscar en la lista de registros pendientes por crear
        if(!modeloToCreateList.isEmpty()){
            Optional<Modelo> temp = modeloToCreateList.stream().filter(modelo -> modelo.getNombre()
                    .equalsIgnoreCase(nombreModelo)).findFirst();
            if(temp != null && temp.isPresent()){
                return temp.get();
            }
        }

        //sino esta en la lista ni en la db, crear el registro nuevo
        Modelo modelo = new Modelo();
        modelo.setNombre(nombreModelo);
        modelo.setDescripcion(nombreModelo);
        modeloToCreateList.add(modelo);
        return modelo;
    }

    private Vehiculo getOrCreateVehiculo(Marca marca, Modelo modelo, ExcelRow excelRow){
        Vehiculo oldVehiculo = null;
        Vehiculo vehiculo = null;
        //valida si existe en la lista en cache
        if(!vehiculoList.isEmpty()){
            Optional<Vehiculo> temp = vehiculoList.stream().filter(v -> v.getPlaca().equals(excelRow.getPlaca())).findFirst();
            if(temp != null && temp.isPresent()){
                oldVehiculo = temp.get();
            }
        }

        //valida si existe en la lista pde pendientes por crear/actualizar
        if(vehiculo == null && !vehiculoToCreateList.isEmpty()){
            Optional<Vehiculo> temp = vehiculoToCreateList.stream().filter(v -> v.getPlaca().equals(excelRow.getPlaca())).findFirst();
            if(temp != null && temp.isPresent()){
                vehiculo = temp.get();
            }
        }

        //si ya existe en la lista de pendientes para crear, omitir
        if(vehiculo != null){
            return null;
        }

        //construir nueva instancia
        vehiculo = new Vehiculo(null, excelRow.getColor(), excelRow.getPlaca(), excelRow.getCapacidad(), marca,
                modelo);

        //validar si ya existe en la lista de cache, para copiar el id ya que seria actualizacion
        if(oldVehiculo != null){
            vehiculo.setId(oldVehiculo.getId());
            //validar si hay algun cambio en el registro, sino, omitir
            if(VehiculoDto.fromModel(oldVehiculo).toString().equals(VehiculoDto.fromModel(vehiculo).toString())){
                return null;
            }
        }

        //agregar a la lista de pendientes por crear/actualizar
        vehiculoToCreateList.add(vehiculo);
        return vehiculo;
    }

    private void saveData(){
        if(!marcaToCreateList.isEmpty()){
             marcaDtoList = marcaService.saveList(marcaToCreateList.stream()
                     .map(marca -> MarcaDto.fromModel(marca)).collect(Collectors.toList()));
        }
        if(!modeloToCreateList.isEmpty()){
            modeloDtoList = modeloService.saveList(modeloToCreateList.stream()
                    .map(modelo -> ModeloDto.fromModel(modelo)).collect(Collectors.toList()));
        }
        if(!vehiculoToCreateList.isEmpty()){
            vehiculoDtoList = vehiculoService.saveList(vehiculoToCreateList.stream()
                    .map(vehiculo -> VehiculoDto.fromModel(vehiculo)).collect(Collectors.toList()));
        }
    }

    private void addGroupedError(Integer columnIndex, ExcelRow excelRow,String ErrorType){
        boolean errorExists = false;
        //si ya existe el error, agregar el numero de columna
        for(Map<String, Object> errorByType : errorsByType){
            if(errorByType.get("ErrorType").equals(errorByType)){
                errorByType.put("Rows",errorByType.get("Rows")+","+excelRow.getRowNumber());
                errorExists = true;
                break;
            }
        }
        //sino, crear el mapa del error y agregarlo a la lista
        if(!errorExists){
            Map<String, Object> errorByType = new HashMap<>();
            errorByType.put("ErrorType", errorByType);
            errorByType.put("Rows",excelRow.getRowNumber());
            errorsByType.add(errorByType);
        }
    }

    private void parseBodyErrors(){
        errorsByType.stream().forEach(errorByType ->
                errorList.add("Error: En Filas:["+errorByType.get("Rows")+"]-Descripcion: "+errorByType.get("ErrorType")));
    }

    /**
     *
     * @param fileLocation
     * @return
     * @throws IOException
     */
    private Sheet readFile(String fileLocation) throws IOException {
        FileInputStream file = new FileInputStream(new File(fileLocation));
        Workbook workbook = new XSSFWorkbook(file);
        return workbook.getSheetAt(0);
    }

    private void sendMail(){
        Mail mail = new Mail();
        mail.setMailFrom("l.ortega@innova4j.com");
        mail.setMailTo(email);
        mail.setMailSubject("Resultados de carga de archivo");

        StringBuilder content = new StringBuilder();
        int i;
        if(!errorList.isEmpty()){
            content.append("Lista de Errores: \n");
            i = 0;
            for (String error : errorList) {
                content.append(++i + ": " + error + "\n");
            }
            content.append("\n\n");
        }

        if(!marcaDtoList.isEmpty()){
            content.append("Lista de Marcas Creadas:\n");
            i = 0;
            for(MarcaDto marcaDto : marcaDtoList){
                content.append(++i + ": " + marcaDto.toString()+"\n");
            }
            content.append("\n\n");
        }

        if(!modeloDtoList.isEmpty()){
            content.append("Lista de Modelos Creados:\n");
            i = 0;
            for(ModeloDto modeloDto : modeloDtoList){
                content.append(++i + ": " + modeloDto.toString()+"\n");
            }
            content.append("\n\n");
        }

        if(!vehiculoDtoList.isEmpty()){
            content.append("Lista de Vehiculos Creados:\n");
            i = 0;
            for(VehiculoDto vehiculoDto : vehiculoDtoList){
                content.append(++i + ": " + vehiculoDto.toString()+"\n");
            }
            content.append("\n\n");
        }

        mail.setMailContent(content.toString());
        mailService.sendEmail(mail);
    }
/*
    public void sendEmail(){
        final Email email = DefaultEmail.builder()
                .from(new InternetAddress("mymail@mail.co.uk"))
                .replyTo(new InternetAddress("someone@localhost"))
                .to(Lists.newArrayList(new InternetAddress("someone@localhost")))
                .subject("Lorem ipsum")
                .body("Lorem ipsum dolor sit amet [...]")
                .encoding(Charset.forName("UTF-8")).build();

        emailService.send(email);
    }

 */
}
