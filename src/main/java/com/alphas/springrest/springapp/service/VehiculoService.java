package com.alphas.springrest.springapp.service;

import com.alphas.springrest.springapp.dao.IMarcaRepository;
import com.alphas.springrest.springapp.dao.IModeloRepository;
import com.alphas.springrest.springapp.dao.IVehiculoRepository;
import com.alphas.springrest.springapp.dto.MarcaDto;
import com.alphas.springrest.springapp.dto.ModeloDto;
import com.alphas.springrest.springapp.dto.VehiculoDto;
import com.alphas.springrest.springapp.model.Marca;
import com.alphas.springrest.springapp.model.Modelo;
import com.alphas.springrest.springapp.model.Vehiculo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("vehiculoService")
public class VehiculoService {
    private static final Logger log = LogManager.getLogger(VehiculoService.class);

    //repositorios
    private final IVehiculoRepository vehiculoRepository;
    private final IMarcaRepository marcaRepository;
    private final IModeloRepository modeloRepository;

    //constructor
    @Autowired
    public VehiculoService(IVehiculoRepository vehiculoRepository, IMarcaRepository marcaRepository,
                           IModeloRepository modeloRepository){
        this.vehiculoRepository = vehiculoRepository;
        this.marcaRepository = marcaRepository;
        this.modeloRepository = modeloRepository;
    }

    public List<VehiculoDto> getAll(){
        return vehiculoRepository.findAll().stream().map(VehiculoDto::fromModel).collect(Collectors.toList());
    }

    public VehiculoDto getById(Long id){
        if(id == null){
            return null;
        }

        Optional<Vehiculo> vehiculo = vehiculoRepository.findById(id);
        if(vehiculo == null || !vehiculo.isPresent()) {
            return null;
        }

        return VehiculoDto.fromModel(vehiculo.get());
    }

    public VehiculoDto getByPlaca(String placa){
        if(placa == null){
            return null;
        }

        Optional<Vehiculo> vehiculo = vehiculoRepository.findByPlaca(placa);
        if(vehiculo == null || !vehiculo.isPresent()) {
            return null;
        }

        return VehiculoDto.fromModel(vehiculo.get());
    }

    public VehiculoDto save(VehiculoDto vehiculoDto){
        if(vehiculoDto == null){
            return null;
        }

        /* si el vehiculo viene sin datos de Marca y Modelo, esta incompleto */
        if(vehiculoDto.getMarcaDto() == null || vehiculoDto.getModeloDto() == null){
            return null;
        }

        /* si marca viene sin id del dto, buscar en la db por nombre */
        if(vehiculoDto.getMarcaDto().getId() == null && vehiculoDto.getMarcaDto().getNombre() != null){
            Optional<Marca> marca =  marcaRepository.findByNombre(vehiculoDto.getMarcaDto().getNombre());

            if(marca == null || !marca.isPresent()){
                return null;
            }
            vehiculoDto.setMarcaDto(MarcaDto.fromModel(marca.get()));
        }

        /* si modelo viene sin id del dto, buscar en db por nombre */
        if(vehiculoDto.getModeloDto().getId() == null && vehiculoDto.getModeloDto().getNombre() != null){
            Optional<Modelo> modelo =  modeloRepository.findByNombre(vehiculoDto.getModeloDto().getNombre());

            if(modelo == null || !modelo.isPresent()){
                return null;
            }
            vehiculoDto.setModeloDto(ModeloDto.fromModel(modelo.get()));
        }

        /* si viene sin id del dto validar que sea nuevo para crear,
            o buscar por placa, para actualizar */
        if(vehiculoDto.getId() == null){
            Optional<Vehiculo> temp = vehiculoRepository.findByPlaca(vehiculoDto.getPlaca());
            if(temp == null || !temp.isPresent()) {
                log.info("doesnt exist: ");
                log.info(vehiculoDto.toString());
                return create(vehiculoDto);
            }
            vehiculoDto.setId(temp.get().getId());

            log.info("dto: ");
            log.info(vehiculoDto.toString());
            return update(vehiculoDto);
        }
        return update(vehiculoDto);
    }

    public List<VehiculoDto> saveList(List<VehiculoDto> vehiculoDtoList){
        return vehiculoDtoList.stream().filter(Objects::nonNull).map(vehiculoDto -> save(vehiculoDto))
                .collect(Collectors.toList());
    }

    public VehiculoDto create(VehiculoDto vehiculoDto){
        vehiculoRepository.create(vehiculoDto.getId(),
                vehiculoDto.getPlaca(),
                vehiculoDto.getColor(),
                vehiculoDto.getCapacidad(),
                vehiculoDto.getMarcaDto().getId(),
                vehiculoDto.getModeloDto().getId());
        return getByPlaca(vehiculoDto.getPlaca());
    }

    public VehiculoDto update(VehiculoDto vehiculoDto){
        Integer updatedRows = vehiculoRepository.update(vehiculoDto.getId(),
                vehiculoDto.getPlaca(),
                vehiculoDto.getColor(),
                vehiculoDto.getCapacidad(),
                vehiculoDto.getMarcaDto().getId(),
                vehiculoDto.getModeloDto().getId());
        if(updatedRows != null && updatedRows > 0){
            return vehiculoDto;
        }
        return null;
    }
}