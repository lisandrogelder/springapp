package com.alphas.springrest.springapp.service;

import com.alphas.springrest.springapp.dao.IMarcaRepository;
import com.alphas.springrest.springapp.dto.MarcaDto;
import com.alphas.springrest.springapp.model.Marca;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service("marcaService")
public class MarcaService {
    private static final Logger log = LogManager.getLogger(MarcaService.class);

    //repositorios
    private final IMarcaRepository marcaRepository;

    //constructor
    @Autowired
    public MarcaService(IMarcaRepository marcaRepository){
        this.marcaRepository = marcaRepository;
    }

    public List<MarcaDto> getAll(){
        return marcaRepository.findAll().stream().map(MarcaDto::fromModel).collect(Collectors.toList());
    }

    public MarcaDto getById(Long id){
        if(id == null){
            return null;
        }

        Optional<Marca> marca = marcaRepository.findById(id);
        if(marca == null || !marca.isPresent()) {
            return null;
        }

        return MarcaDto.fromModel(marca.get());
    }

    public MarcaDto getByNombre(String nombre){
        if(nombre == null){
            return null;
        }

        Optional<Marca> marca = marcaRepository.findByNombre(nombre);
        if(marca == null || !marca.isPresent()) {
            return null;
        }

        return MarcaDto.fromModel(marca.get());
    }

    public MarcaDto save(MarcaDto marcaDto){
        if(marcaDto == null){
            return null;
        }

        if(marcaDto.getId() == null){
            Optional<Marca> temp = marcaRepository.findByNombre(marcaDto.getNombre());
            if(temp == null || !temp.isPresent()) {
                log.info("doesnt exist: ");
                log.info(marcaDto.toString());
                return create(marcaDto);
            }
            marcaDto.setId(temp.get().getId());

            log.info("dto: ");
            log.info(marcaDto.toString());
            return update(marcaDto);
        }
        return update(marcaDto);
    }

    public List<MarcaDto> saveList(List<MarcaDto> marcaDtoList){
        return marcaDtoList.stream().filter(Objects::nonNull).map(marcaDto -> save(marcaDto))
                .collect(Collectors.toList());
    }

    public MarcaDto create(MarcaDto marcaDto){
        marcaRepository.create(marcaDto.getId(), marcaDto.getNombre(), marcaDto.getDescripcion());
        return getByNombre(marcaDto.getNombre());
    }

    public MarcaDto update(MarcaDto marcaDto){
        Integer updatedrows = marcaRepository.update(marcaDto.getId(), marcaDto.getNombre(), marcaDto.getDescripcion());
        if(updatedrows != null && updatedrows > 0){
            return marcaDto;
        }
        return null;
    }
}
