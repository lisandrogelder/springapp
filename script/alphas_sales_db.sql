SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";

-- 
-- Base de datos: `alphas_sales_db`
-- 

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `marca`
-- 

CREATE TABLE `marca` (
  `id_marca` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(256) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_marca`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `marca`
-- 

INSERT INTO `marca` VALUES (1, 'marca prueba 1', 'prueba 1');
INSERT INTO `marca` VALUES (2, 'marca prueba 2', 'prueba 2');
INSERT INTO `marca` VALUES (3, 'marca prueba 3', 'prueba 3');
INSERT INTO `marca` VALUES (4, 'Marca Prueba 4', 'Marca Prueba 4');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `modelo`
-- 

CREATE TABLE `modelo` (
  `id_modelo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(256) NOT NULL,
  `descripcion` text,
  PRIMARY KEY (`id_modelo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `modelo`
-- 

INSERT INTO `modelo` VALUES (1, 'modelo prueba 1', ' modelo prueba 1 modificado');
INSERT INTO `modelo` VALUES (2, 'modelo prueba 2', ' modelo prueba 2');
INSERT INTO `modelo` VALUES (3, 'modelo prueba 3', ' modelo prueba 3');
INSERT INTO `modelo` VALUES (4, 'Modelo Prueba 4', 'Modelo Prueba 4');

-- --------------------------------------------------------

-- 
-- Estructura de tabla para la tabla `vehiculo`
-- 

CREATE TABLE `vehiculo` (
  `id_vehiculo` int(11) NOT NULL AUTO_INCREMENT,
  `color` varchar(256) NOT NULL,
  `placa` varchar(256) NOT NULL,
  `capacidad` double NOT NULL,
  `id_modelo` int(11) NOT NULL,
  `id_marca` int(11) NOT NULL,
  PRIMARY KEY (`id_vehiculo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

-- 
-- Volcar la base de datos para la tabla `vehiculo`
-- 

INSERT INTO `vehiculo` VALUES (1, 'azul', '123', 100, 1, 1);
INSERT INTO `vehiculo` VALUES (2, 'Verde', '456', 14, 2, 2);
INSERT INTO `vehiculo` VALUES (3, 'Negro', '789', 19, 3, 3);
INSERT INTO `vehiculo` VALUES (4, 'Rojo', '12', 24, 4, 4);